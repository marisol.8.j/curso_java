import javax.print.attribute.standard.JobMessageFromOperator;
import javax.swing.*;

public class SistemasNumericos {
    public static void main(String[] args) {

        String numeroStr = JOptionPane.showInputDialog(null, "Ingrese un número entero");
        int NumeroDecimal = 0;
        try { NumeroDecimal = Integer.parseInt(numeroStr);

        } catch (NumberFormatException e){
            JOptionPane.showMessageDialog(null,"Error debe ingresar un Número entero");
            main(args);
            System.exit(0);
        }


        System.out.println("NumeroDecimal = " + NumeroDecimal);

        String ResultadoBinario = "numero binario de = " + NumeroDecimal + " = " + Integer.toBinaryString(NumeroDecimal);
        System.out.println(ResultadoBinario);

        int NumeroBinario = 0b11110;
        System.out.println("NumeroBinario = " + NumeroBinario);

        String ResultadoOctal = "numero octal de = " + NumeroDecimal + " = " + Integer.toOctalString(NumeroDecimal);
        System.out.println(ResultadoOctal);

        int NumeroOctal = 036;
        System.out.println("NumeroOctal = " + NumeroOctal);

        String ResultadoHex = "numero hexadecimal de " + NumeroDecimal + " = " + Integer.toHexString(NumeroDecimal);
        System.out.println(ResultadoHex);

        int NumeroHex = 0x1e;
        System.out.println("NumeroHex = " + NumeroHex);

        String mensaje = (ResultadoBinario);
        mensaje += "\n" + ResultadoOctal;
        mensaje += "\n" + ResultadoHex;

        JOptionPane.showMessageDialog(null,mensaje);



    }
}
