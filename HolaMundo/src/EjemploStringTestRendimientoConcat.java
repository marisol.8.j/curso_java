public class EjemploStringTestRendimientoConcat {
    public static void main(String[] args) {

        String a = "a";
        String b = "b";
        String c = "a";

        StringBuilder sb = new StringBuilder(a);
        Long inicio = System.currentTimeMillis();

        for (int i = 0; i < 100000; i++) {
             // c = c.concat(a).concat(b).concat(" \n "); // 500 => 2ms, 1000 => 5ms, 10000 => 158ms, 100000 => 10194ms
            // c += a + b + "\n"; // 500 => 13, 1000 => 0ms, 1000 => 11ms, 10000 => 54ms, 100000 => 3108ms
            // sb.append(a).append(b).append("\n"); // 500 => 0ms, 1000 => 0ms, 10000 => 2ms, 100000 => 13ms
        }

        Long fin = System.currentTimeMillis();
        System.out.println( fin - inicio );
        System.out.println("c = " + c);
        System.out.println("sb = " + sb.toString());



    }
}
