import java.util.Scanner;

public class ConversionDeTipos {
    public static void main(String[] args) {
        String numeroStr = "50";

        int numeroInt = Integer.parseInt(numeroStr);
        System.out.println("numeroInt = " + numeroInt);

        String realStr = "98765.43";
        double realDouble = Double.parseDouble(realStr);
        System.out.println("realDouble = " + realDouble);

        String logicoStr = "true";
        boolean realBoolean = Boolean.getBoolean(logicoStr);
        System.out.println("realBoolean = " + realBoolean);

        int OtroNumeroInt = 100;

        System.out.println("OtroNumeroInt = " + OtroNumeroInt);

        String OtroNUmeroStr = Integer.toString(OtroNumeroInt);
        System.out.println("OtroNUmeroStr = " + OtroNUmeroStr);

        OtroNUmeroStr= String.valueOf(OtroNumeroInt+10);
        System.out.println("OtroNUmeroStr = " + OtroNUmeroStr);

        Double OtroRealDouble = 1.23456;
        String OtroRealStr = Double.toString(OtroRealDouble);
        System.out.println("OtroRealStr = " + OtroRealStr);

        OtroRealStr =String.valueOf(1.23456e2f);
        System.out.println("OtroRealStr = " + OtroRealStr);

        int i = 22768;
        short s = (short) i;
        System.out.println("s = " + s);
        long l= i;
        System.out.println("l = " + l);
        System.out.println(Short.MAX_VALUE);
        char b = (char) i;
        System.out.println("b = " + b);
        float f = (float) i;
        System.out.println("f = " + f);








    }
}
