public class EjemploStringMetodos {
    public static void main(String[] args) {

        String nombre = "Marisol";

        System.out.println("nombre.length() = " + nombre.length());
        System.out.println("nombre.toUpperCase() = " + nombre.toUpperCase());
        System.out.println("nombre.toLowerCase() = " + nombre.toLowerCase());
        System.out.println("nombre.equals(\"Marisol\") = " + nombre.equals("Marisol"));
        System.out.println("nombre.equals(\"marisol\") = " + nombre.equals("marisol"));
        System.out.println("nombre.equalsIgnoreCase(\"marisol\") = " + nombre.equalsIgnoreCase("marisol"));
        System.out.println("nombre.compareTo(\"Marisol\") = " + nombre.compareTo("Marisol"));
        System.out.println("nombre.compareTo(\"Adiel\") = " + nombre.compareTo("Adiel"));
        System.out.println("nombre.charAt(0) = " + nombre.charAt(0));
        System.out.println("nombre.charAt(1) = " + nombre.charAt(1));
        System.out.println("nombre.charAt(nombre.length()-1) = " + nombre.charAt(nombre.length()-1));
        System.out.println("nombre.charAt(4) = " + nombre.charAt(4));

        System.out.println("nombre.substring(1) = " + nombre.substring(1));
        System.out.println("nombre.substring(1, 4) = " + nombre.substring(1, 4));
        System.out.println("nombre.substring(4, 6) = " + nombre.substring(4, 6));
        System.out.println("nombre.substring(nombre.length()-1) = " + nombre.substring(nombre.length()-1));
        
        String trabalenguas = "trabalenguas";
        System.out.println("trabalenguas.replace(\"a\",\".\") = " + trabalenguas.replace("a","."));
        System.out.println("trabalenguas = " + trabalenguas);
        System.out.println("trabalenguas.indexOf(\"a\") = " + trabalenguas.indexOf("a"));
        System.out.println("trabalenguas.lastIndexOf(\"a\") = " + trabalenguas.lastIndexOf("a"));
        System.out.println("trabalenguas.indexOf(\"z\") = " + trabalenguas.indexOf("lenguas"));
        System.out.println("trabalenguas.contains(\"t\") = " + trabalenguas.contains("lenguas"));
        System.out.println("trabalenguas.startsWith(tr) = " + trabalenguas.startsWith("tr"));
        System.out.println("trabalenguas.endsWith(tr) = " + trabalenguas.endsWith("s"));
        System.out.println(" trabalenguas ");
        System.out.println(" trabalenguas ".trim());





        








    }
}
