import javax.swing.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class SistemasNumericosEntradaScanner {
    public static void main(String[] args) {

        Scanner Scanner = new Scanner(System.in);
        System.out.println("Ingrese un número entero");
        // String numeroStr = Scanner.nextLine();
        int NumeroDecimal = 0;
        try {
            NumeroDecimal = Scanner.nextInt(); // Integer.parseInt(numeroStr);

        } catch (InputMismatchException e){
            System.out.println("Error debe ingresar un Número entero");
            main(args);
            System.exit(0);
        }


        System.out.println("NumeroDecimal = " + NumeroDecimal);

        String ResultadoBinario = "numero binario de = " + NumeroDecimal + " = " + Integer.toBinaryString(NumeroDecimal);


        String ResultadoOctal = "numero octal de = " + NumeroDecimal + " = " + Integer.toOctalString(NumeroDecimal);


        String ResultadoHex = "numero hexadecimal de " + NumeroDecimal + " = " + Integer.toHexString(NumeroDecimal);


        String mensaje = (ResultadoBinario);
        mensaje += "\n" + ResultadoOctal;
        mensaje += "\n" + ResultadoHex;

        System.out.println(mensaje);



    }
}
